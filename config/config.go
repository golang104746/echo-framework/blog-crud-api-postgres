package config

import "fmt"

// all env variables available here and are validated via envconfig
// specifies all the below env variables are required, so removing any from .env will cause an error
type EnvConfig struct {
	AppName string `envconfig:"APP_NAME" required:"true"` // error will be thown if required is set to true and the variable is not set in .env
	AppHost string `envconfig:"APP_HOST" required:"true"`
	AppPort string `envconfig:"APP_PORT" required:"true"`

	Environment string `envconfig:"ENVIRONMENT" required:"true"`
	LogLevel    string `envconfig:"LOG_LEVEL" required:"true"`

	DBDriver  string `envconfig:"DB_DRIVER" required:"true"`
	DBHost    string `envconfig:"DB_HOST" required:"true"`
	DBPort    string `envconfig:"DB_PORT" required:"true"`
	DBSSLMode string `envconfig:"DB_SSLMODE" required:"true"`
	DBName    string `envconfig:"DB_NAME" required:"true"`
	DBUser    string `envconfig:"DB_USER" required:"true"`
	DBPass    string `envconfig:"DB_PASS" required:"true"`
}

// postgres connection string
func (c *EnvConfig) DataSourceName() string {
	// ${DB_DRIVER}://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=${DB_SSLMODE}
	// replace variables with the format specifier string
	// %s://%s:%s@%s:%s/%s?sslmode=%s
	return fmt.Sprintf("%s://%s:%s@%s:%s/%s?sslmode=%s", c.DBDriver, c.DBUser, c.DBPass, c.DBHost, c.DBPort, c.DBName, c.DBSSLMode)
}
