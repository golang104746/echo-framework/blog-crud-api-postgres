package main

import (
	"fmt"
	"github.com/aidan/echo-blog/config"
	"github.com/jmoiron/sqlx"
	"net/http"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	_ "github.com/lib/pq" // _ we dont need to call anything from the library, we just need to do everything from its init function, let it do the magic from its init function
)

// response obj message
type Response struct {
	Message string `json:"message"`
}

func main() {
	// loads .env file by default
	if err := godotenv.Load(); err != nil {
		log.Fatal(err)
	}

	// reads env variables
	var cfg config.EnvConfig
	if err := envconfig.Process("", &cfg); err != nil {
		log.Fatal(err)
	}
	//err := envconfig.Process("", &cfg)
	//if err != nil {
	//	log.Fatal(err.Error())
	//}

	// test env variables are working
	fmt.Println(cfg.AppName)

	// connect to the database
	db, err := sqlx.Connect(cfg.DBDriver, cfg.DataSourceName())
	if err != nil {
		log.Fatal(err)
	}

	// if we aren't able to ping the db, throw error
	if err := db.Ping(); err != nil {
		log.Fatal(err)
	}

	e := echo.New()              // create instance of echo framework
	e.Logger.SetLevel(log.DEBUG) // determines the severity of the events that the app will log
	e.Use(middleware.Logger())   // log endpoint calls
	e.Use(middleware.Recover())  // recover from panics gracefully - handles displaying json error message instead of broken app

	e.GET("/hello-world", func(c echo.Context) error {
		// panic("your program has a panic")
		// return c.String(http.StatusOK, "Hello world..")
		res := Response{
			Message: "Hello world..",
		}

		return c.JSON(http.StatusOK, res)
	})

	e.Logger.Fatal(e.Start(fmt.Sprintf("%s:%s", cfg.AppHost, cfg.AppPort)))
	// e.Start("127.0.0.1:8080") // or localhost:8080 does the same thing
}
