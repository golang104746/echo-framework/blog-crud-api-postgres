package repositories

import (
	"github.com/aidan/echo-blog/entities"
	"github.com/jmoiron/sqlx"
)

// user_repository - gets injected into , if we test the service we need to have a mock for the user_repository

// UserRepository provides actions to manipulate users in the database.
type UserRepository interface {
	User(ID int) (entities.User, error)
	Users() ([]entities.User, error)
	Insert(user entities.User) error
	Update(user entities.User) error
	Delete(ID int) error
}

// userRepo implements the UserRepository interface it should satisfy all the methods declared in UserRepository
// This means that userRepo should have methods for tasks like creating a new user, getting a user by id, updating a user, etc.
// composite data type that groups together zero or more items of possibly different types into a single entity.
type userRepo struct {
	db *sqlx.DB // pointer to sqlx db
}

// an instance of the db connection is passed in when called from main.go
// func NewUserRepo(db *sqlx.DB) *userRepo {
//	return &userRepo{
//		db: db,
//	}
// }

// NewUserRepo instantiates userRepo struct - used in main.go to initalize the user repo
// abstraction over the db for performing operations related to User

// func NewUserRepo(db *sqlx.DB) UserRepository {...}: NewUserRepo is a function that takes
// a pointer to an sqlx.DB object as an argument. sqlx.DB is a database handle representing a pool of zero or more
// underlying connections from the sqlx library, which is an extension of the standard database/sql library in Go. This function returns an instance of an interface called UserRepository.

// In summary, you can think of the NewUserRepo function as a constructor for your User Repository - it's a function you call to create a new instance of UserRepository.
// This set-up is typical in applications that follow the Repository Design Pattern, enabling loose coupling and better code structuring.
func NewUserRepo(db *sqlx.DB) UserRepository {
	// passes a database connection to it

	// creates a new instance of a userRepo struct
	// initializes its db field with the db argument passed to NewUserRepo, and returns a pointer to this new instance.
	return &userRepo{
		db: db,
	}
}

// User fetches a user from the database and return it.
// receives id from user and use it to query the db, ideally a service will call this repository method
func (r *userRepo) User(ID int) (entities.User, error) {
	var user entities.User
	err := r.db.Get(&user, "SELECT * FROM users WHERE id = $1", ID)
	return user, err
}

// Users fetches all the users from the database and return them.
func (r *userRepo) Users() ([]entities.User, error) {
	var users []entities.User
	// Select function from sqlx db
	err := r.db.Select(&users, "SELECT * FROM users")
	return users, err
}

// Insert inserts a new user into the database.
func (r *userRepo) Insert(user entities.User) error {
	// named parameters to pass the user struct, automatically injects the values replacing these named parameters
	_, err := r.db.NamedExec("INSERT INTO users (firstname, lastname, email, status) VALUES (:firstname, :lastname, :email, :status)", user)
	if err != nil {
		return err
	}

	return nil
}

// Update updates given user in the database.
func (r *userRepo) Update(user entities.User) error {
	_, err := r.db.NamedExec("UPDATE users SET firstname = :firstname, lastname = :lastname, email = :email, status = :status) WHERE id = :id", user)
	if err != nil {
		return err
	}

	return nil
}

// Delete removes a user from the database.
func (r *userRepo) Delete(ID int) error {
	_, err := r.db.Exec("DELETE FROM users WHERE id = $1", ID)
	if err != nil {
		return err
	}

	return nil
}
